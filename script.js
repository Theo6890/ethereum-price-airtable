// Change this to the name of a table in your base
let table = base.getTable('Crypto - Bootstrap funding');

let result = await table.selectRecordsAsync();


for (let record of result.records) {
    // Retrieve UNIX time from cell
    let from = record.getCellValue('Buy time - GMT');
    let to = record.getCellValue('Buy time - GMT') + 300;

    // Use coingecko historic price
    let base_url = `https://api.coingecko.com/api/v3/coins/ethereum/market_chart/range?vs_currency=usd&from=${from}&to=${to}`;
    let apiResponse = await fetch(base_url);
    let data = await apiResponse.json().then(result => result.prices[0][1]);
    output.text(`Conversion rate: ${data}`);

    // update 'Dollar price in time'
    await table.updateRecordAsync(record, {
        // Change these names to fields in your base
        'Dollar price at time': data,
    });
}
